# ManyLinesQQuickPaintedItem

painting with QQuickPaintedItem component

отрисовка с QPainter

10 000 линий по две точки в каждой 0 seconds 845 milliseconds
100 000 линий по две точки в каждой 8 seconds 241 milliseconds
1 000 000 линий по две точки в каждой 81 seconds 383 milliseconds


Отрисовка количества путей. В каждом пути 50 точек
количество путей -- время отрисовки

Два новых подхода.
1) Отрисовка 50 точек, 25 линий  ( paintlines(int number, QPainter *painter))
2) Отрисовка 1 линии из 50 точек, с помощью drawPolyline 
( paintPolygon(int number, QPainter *painter))


Количество отрисовывемого
количество линий(кол-во итераций(отрисовка по 50 точек)) - подход 1 - подход 2
100 - 0 seconds 219 milliseconds - 13 seconds 552 milliseconds
1000 - 2 seconds 139 milliseconds - 174 seconds 545 milliseconds (2,9 min)
10 000 -  19 seconds 971 milliseconds - ... > 30 min
100 000 -  197 seconds 934 milliseconds (3.2 min)



