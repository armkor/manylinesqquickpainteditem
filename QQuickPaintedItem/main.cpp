#include "manylines.h"
#include <QtQuick/QQuickView>
#include <QGuiApplication>

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	qmlRegisterType<ManyLines>("LineGenerator", 1, 0, "ManyLines");

	QQuickView view;
	view.setResizeMode(QQuickView::SizeRootObjectToView);
	view.setSource(QUrl("qrc:/main.qml"));
	view.show();
	return app.exec();
}
