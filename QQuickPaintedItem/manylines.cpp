#include "manylines.h"
#include <QVector>
#include <algorithm>

ManyLines::ManyLines(QQuickItem *parent)
	: QQuickPaintedItem(parent)
{
	int testingNumber = 1000;

	int width = 1200;
	int height = 1000;
	int x, y;

	data.reserve(testingNumber);
	for(int i = 0; i < testingNumber; i++)
	{
		QVector<QPoint> onePath;
		onePath.reserve(50);
		for (int j = 0; j < 50; j++)
		{
			x = rand() % width;
			y = rand() % height;
			onePath.push_back(QPoint(x, y));
		}
		data.push_back(onePath);
	}
}

void ManyLines::paint(QPainter *painter)
{
	QElapsedTimer timer;
	timer.start();
	paintPolyline(painter);
	qint64 mytime = timer.elapsed();
	qDebug() << "############### The operation took" << mytime / 1000 << "seconds" << mytime % 1000 << "milliseconds";

}


void ManyLines::paintLines(QPainter *painter)
{
	QBrush brush(QColor("#007430"));

	QColor colours[10] = {QColor("cyan"), QColor("magenta"), QColor("red"),
						  QColor("darkRed"), QColor("darkCyan"), QColor("darkMagenta"),
						  QColor("green"), QColor("darkGreen"), QColor("yellow"),
						  QColor("blue")};

	painter->setBrush(brush);
	QPen pen;
	painter->setRenderHint(QPainter::Antialiasing);

	int i = 0, j = 0;

	while (i < data.size())
	{
		pen.setWidth(3);
		pen.setColor(colours[rand() % 9]);

		painter->setPen(pen);

		j = 0;
		while (j < data[i].size() - 1)
		{
			painter->drawLine(data[i][j], data[i][j + 1]);
			j++;
		}
		i++;
	}
}

void ManyLines::paintPolyline(QPainter *painter)
{
	QBrush brush(QColor("#007430"));


	QColor colours[10] = {QColor("cyan"), QColor("magenta"), QColor("red"),
						  QColor("darkRed"), QColor("darkCyan"), QColor("darkMagenta"),
						  QColor("green"), QColor("darkGreen"), QColor("yellow"),
						  QColor("blue")};


	painter->setBrush(brush);
	QPen pen;
	pen.setWidth(3);
	painter->setPen(pen);
	painter->setRenderHint(QPainter::Antialiasing);

	int i = 0;

	while (i < data.size())
	{
		pen.setWidth(3);
		pen.setColor(colours[rand() % 9]);

		painter->setPen(pen);
		painter->drawPolyline(data[i]);
		i++;
	}
}
