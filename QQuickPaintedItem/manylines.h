#include <QtQuick>

#include <QVector>
#include <QPoint>

class ManyLines : public QQuickPaintedItem
{
	Q_OBJECT

	public:
		ManyLines(QQuickItem *parent = 0);
		void paint(QPainter *painter);

	private:
		void paintLines(QPainter *painter);
		void paintPolyline(QPainter *painter);

		QVector<QVector<QPoint> > data;

};
